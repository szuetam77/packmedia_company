package pl.java.company_app.service;

import org.springframework.stereotype.Service;
import pl.java.company_app.entity.Product;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public interface ProductService {


    public List<Product> getAllProducts();

    public Optional<Product> getProductById(Long id);

    public Product createProduct(Product product);

    public void deleteProduct(Long id);

    public void updateProduct(Product product);

    public List<Product> findByCategoryId(Long id);


}
