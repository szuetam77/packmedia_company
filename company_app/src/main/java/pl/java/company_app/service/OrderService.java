package pl.java.company_app.service;

import org.springframework.stereotype.Service;
import pl.java.company_app.entity.Order;

import java.util.List;
import java.util.Optional;

@Service
public interface OrderService {

    public List<Order> getAllOrders();

    public Optional<Order> getOrderById(Long id);

    public Order createOrder(Order order);

    public void deleteOrder(Long id);
}
