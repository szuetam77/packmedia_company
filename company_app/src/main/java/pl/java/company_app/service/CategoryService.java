package pl.java.company_app.service;

import org.springframework.stereotype.Service;
import pl.java.company_app.entity.Category;
import pl.java.company_app.entity.Product;

import java.util.List;
import java.util.Optional;

@Service
public interface CategoryService {

    public List<Category> getAllCategories();

    public Optional<Category> getCategoryById(Long id);

    public Category createCategory(Category category);

    public void deleteCategory(Long id);

    public void updateCategory(Category category);

}
