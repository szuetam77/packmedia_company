package pl.java.company_app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.java.company_app.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
