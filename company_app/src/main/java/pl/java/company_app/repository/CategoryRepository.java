package pl.java.company_app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.java.company_app.entity.Category;
import pl.java.company_app.entity.Product;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {


}
