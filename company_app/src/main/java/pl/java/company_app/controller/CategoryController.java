package pl.java.company_app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.java.company_app.assembler.CategoryModelAssembler;
import pl.java.company_app.entity.Category;
import pl.java.company_app.entity.Product;
import pl.java.company_app.exception.CategoryNotFoundException;
import pl.java.company_app.service.CategoryService;
import pl.java.company_app.service.ProductService;

import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequiredArgsConstructor
public class CategoryController {


    private final CategoryService categoryService;
    private final CategoryModelAssembler categoryModelAssembler;
    private final ProductService productService;


    @GetMapping("/categories/{category_id}/products")
    public List<Product> getProductFromCategory(@PathVariable Long category_id) {
        return productService.findByCategoryId(category_id);
    }


    @GetMapping("/categories")
    public CollectionModel<EntityModel<Category>> getAllCategories() {
        List<EntityModel<Category>> categories = categoryService.getAllCategories().stream()
                .map(categoryModelAssembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(categories,
                linkTo(methodOn(CategoryController.class).getAllCategories()).withSelfRel());
    }

    @GetMapping("/category/{id}")
    public EntityModel<Category> getCategoryByID(@PathVariable Long id) {

        Category category = categoryService.getCategoryById(id)
                .orElseThrow(() -> new CategoryNotFoundException(id));

        return categoryModelAssembler.toModel(category);
    }

    @PostMapping("/category")
    public ResponseEntity<?> newCategory(@RequestBody Category category) throws URISyntaxException {

        EntityModel<Category> entityModel = categoryModelAssembler
                .toModel(categoryService.createCategory(category));

        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF)
                .toUri())
                .body(entityModel);
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable Long id) {
        categoryService.deleteCategory(id);

        return ResponseEntity.noContent().build();
    }


}
