package pl.java.company_app.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.java.company_app.assembler.ProductModelAssembler;
import pl.java.company_app.entity.Product;
import pl.java.company_app.exception.ProductNotFoundException;
import pl.java.company_app.repository.ProductRepository;
import pl.java.company_app.service.ProductService;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequiredArgsConstructor
public class ProductController {



    private final ProductService productService;
    private final ProductModelAssembler productModelAssembler;
    private final ProductRepository productRepository;


    @GetMapping("/products")
    public CollectionModel<EntityModel<Product>> getAllProducts() {
        List<EntityModel<Product>> products = productService.getAllProducts().stream()
                .map(productModelAssembler::toModel)
                .collect(Collectors.toList());

        return new CollectionModel<>(products,
                linkTo(methodOn(ProductController.class).getAllProducts()).withSelfRel());
    }

    @GetMapping("/product/{id}")
    public EntityModel<Product> getProductByID(@PathVariable Long id) {

        Product product = productService.getProductById(id)
                .orElseThrow(() -> new ProductNotFoundException(id));

        return productModelAssembler.toModel(product);
    }

    @PostMapping("/product")
    public ResponseEntity<?> newProduct(@RequestBody Product product) {

        EntityModel<Product> entityModel = productModelAssembler
                .toModel(productService.createProduct(product));

        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF)
                .toUri()).body(entityModel);
    }

    @PutMapping("/product/{id}")
    public ResponseEntity<?> updateProduct(@RequestBody Product updatedProduct, @PathVariable Long id) throws ProductNotFoundException {
        Product product = productService.getProductById(id)
                .map(p -> {
                    p.setName(updatedProduct.getName());
                    p.setDescription(updatedProduct.getDescription());
                    p.setPrice(updatedProduct.getPrice());
                    return productRepository.save(p);
                })
                .orElseGet(() -> {
                    updatedProduct.setId(id);
                    return productRepository.save(updatedProduct);
                });

        EntityModel<Product> entityModel = productModelAssembler.toModel(product);

        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF)
                .toUri())
                .body(entityModel);
    }

    @DeleteMapping("/product/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);

        return ResponseEntity.noContent().build();
    }

}
