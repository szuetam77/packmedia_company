package pl.java.company_app.exception;

public class ProductNotFoundException extends RuntimeException {

    public ProductNotFoundException(Long id){
        super("Couldn't find ID: " + id);
    }
}
