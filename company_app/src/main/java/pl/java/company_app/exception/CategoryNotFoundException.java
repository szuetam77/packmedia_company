package pl.java.company_app.exception;

public class CategoryNotFoundException  extends RuntimeException {

    public CategoryNotFoundException(Long id){
        super("Couldn't find ID: " + id);
    }
}
