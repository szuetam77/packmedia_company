package pl.java.company_app.exception;

public class OrderNotFoundException extends RuntimeException {

    public OrderNotFoundException(Long id){
        super("Couldn't find ID: " + id);
    }
}
