package pl.java.company_app.assembler;


import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;
import pl.java.company_app.controller.CategoryController;
import pl.java.company_app.entity.Category;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@Component
public class CategoryModelAssembler implements RepresentationModelAssembler
        <Category, EntityModel<Category>> {


    @Override
    public EntityModel<Category> toModel(Category category) {

        return new EntityModel<>(category,
                linkTo(methodOn(CategoryController.class).getCategoryByID(category.getId())).withSelfRel(),
                linkTo(methodOn(CategoryController.class).getAllCategories()).withRel("categories"));
    }
}
