package pl.java.company_app.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long id;
    @Column(name = "order_company_name")
    private String companyName;
    @Column(name = "order_delivery", columnDefinition = "DATE")
    private LocalDate deliveryTime;
    @OneToMany(mappedBy = "order")
    @JsonIgnore
    private List<Product> products;
    @Column(name = "order_status")
    @Enumerated(EnumType.STRING)
    private Status status;
}
