package pl.java.company_app.entity;

public enum Status {
    IN_PROGRESS,
    COMPLETED,
    CANCELLED;
}
