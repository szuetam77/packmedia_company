$(document).ready(function() {

    $.ajax({
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        url: window.location + "products"
    }).done(function(data) {
        $('#div1').append(data._embedded.products[0].id)
        $('#div1').append(data._embedded.products[0].name)
        $('#div1').append(data._embedded.products[0].price)
        $('#div1').append(data._embedded.products[0].description)
    });

    $.ajax({
            type: "GET",
            contentType: "application/json",
            dataType: "json",
            url: window.location + "orders"
        }).done(function(data) {
            $('#div2').append(data._embedded.orders[0].id)
            $('#div2').append(data._embedded.orders[0].companyName)
            $('#div2').append(data._embedded.orders[0].status)
        });

        $.ajax({
                type: "GET",
                contentType: "application/json",
                dataType: "json",
                url: window.location + "categories"
            }).done(function(data) {
                $('#div3').append(data._embedded.categories[0].id)
                $('#div3').append(data._embedded.categories[0].name)
                $('#div3').append(data._embedded.categories[0].productList)
            });
});